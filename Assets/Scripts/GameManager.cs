﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	public int gameState = 0;

	public int maxMonsters = 20;
	public int currentMonsters = 0;
	
	public Camera camGame;
	public Camera camBattle1;
	
	private BattleManager bm;
	
	// Use this for initialization
	void Start () 
	{
		bm = gameObject.GetComponent<BattleManager>();
		camGame.enabled = true;
		camBattle1.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (gameState == 0)
		{
		
		}
		else if(gameState == 1)
		{
			if(Input.GetKeyUp(KeyCode.Space)|| Input.GetKeyUp(KeyCode.Joystick1Button0))
			{
				bm.EndBattle();
			}
			
		}
		
	}
	
	public void StartBattle(int species, int terrainType)
	{
		camGame.enabled = false;
		camBattle1.enabled = true;
		
		gameState = 1;
		
		bm.StartBattle (species);
	}
	
	public void EndBattle()
	{
		gameState = 0;
		camGame.enabled = true;
		camBattle1.enabled = false;
	}
	
	public void ResetPlayer()
	{
	
	}
}
