﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour 
{
	//private Vector3 startPoint;
	private Vector3 dir;
	public float speed;
	private bool inWater = false;
	private CharacterController control;
	public ParticleEmitter waterSplash;
	public GameObject charMesh;
	
	// Use this for initialization
	void Start () 
	{
		//startPoint = transform.position;
		dir = Vector3.zero;
		control = gameObject.GetComponent<CharacterController>();	
		//speed = 10;
		waterSplash.emit = false;
		control.SimpleMove(transform.forward * (speed));
	}
	
	// Update is called once per frame
	void Update () 
	{
		dir.x = -1 * Input.GetAxis ("Horizontal");
		dir.z = -1 * Input.GetAxis ("Vertical");
		
		//Debug.Log (inWater);
		
		if(dir.x != 0 || dir.z != 0)
		{
			transform.forward = dir;
			control.SimpleMove(transform.forward * speed);

			if(!control.isGrounded)
			{
				control.Move (Vector3.down);
			}
			
			if(charMesh != null)
			{
				if(inWater)
				{
					charMesh.GetComponent<Animation>().CrossFade ("Swim");
				}
				else
				{	
					charMesh.GetComponent<Animation>().CrossFade ("Walk");
				}
			}
		}
		else
		{
			if(charMesh != null)
			{
				charMesh.GetComponent<Animation>().CrossFade ("Idle");
			}
		}
		
		
	}
	
	
	void OnCollisionEnter (Collision collision)
	{
		//Debug.Log ("Collide with " + collision.gameObject.name);
	}
	
	void OnTriggerEnter(Collider collision)
	{
		//Debug.Log ("Triggered by " + collision.gameObject.name);
		if(collision.gameObject.name == "WaterTrigger")
		{
			inWater = true;
			waterSplash.emit = true;
		}
	}
	
	void OnTriggerExit(Collider collision)
	{
		if(collision.gameObject.name == "WaterTrigger")
		{
			inWater = false;
			waterSplash.emit = false;
		}
	}
	
	void OnControllerColliderHit(ControllerColliderHit collision)
	{
		if(collision.gameObject.name == "Terrain")
		{
			inWater = false;
			waterSplash.emit = false;
		}
		else if(collision.gameObject.name == "WaterCollider")
		{
			inWater = true;
			waterSplash.emit = true;
		}
	}
	
}
