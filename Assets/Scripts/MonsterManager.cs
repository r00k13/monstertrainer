﻿using UnityEngine;
using System.Collections;

public class MonsterManager : MonoBehaviour 
{
	public Monster[] partyMonsters = new Monster[6];
	public Monster[] storedMonsters = new Monster[100];
	
	// Use this for initialization
	void Start () 
	{
		partyMonsters[0] = new Monster(0, 5);
		//partyMonsters[1] = new Monster(1, 5);
		if (partyMonsters[2] == null)
		{
			Debug.Log ("2 empty");
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	public int CheckSpares()
	{
		int spares = 0;
		for(int i = 0; i < 6; i++)
		{
			if(partyMonsters[i] != null)
			{
				if(partyMonsters[i].currentHealth > 0)
				{
					spares++;
				}
			}
		}
		return spares;
	}
	
	public void Restore()
	{
		for(int i = 0; i < 6; i++)
		{
			if(partyMonsters[i] != null)
			{
				partyMonsters[i].currentHealth = partyMonsters[i].maxHealth;
			}
		}
	}
}

public class Monster
{
	public int speciesID;
	public int lvl;
	
	public int exp;
	
	public string name;
	public int maxHealth;
	public int currentHealth;
	public int strength;
	public int toughness;
	public int speed;
	
	public int resistance;								//0 = physical, 1 = flame, 2 = aqua, 3 = venom, 4 = shadow, 5 = volt
	public int weakness;
	
	public int[] attacks = new int[3];
	
	private MonsterStats stats = new MonsterStats();

	public Monster(int s, int l)
	{
		speciesID = s;
		lvl = l;
		
		name = stats.monsterDetails[speciesID].name;
		maxHealth = (int)(lvl * stats.monsterDetails[speciesID].healthMod);
		strength = (int)(lvl * stats.monsterDetails[speciesID].strengthMod);
		toughness = (int)(lvl * stats.monsterDetails[speciesID].toughMod);
		speed = (int)(lvl * stats.monsterDetails[speciesID].speedMod);
		resistance = (int)(lvl * stats.monsterDetails[speciesID].res);
		weakness = (int)(lvl * stats.monsterDetails[speciesID].weak);
		
		currentHealth = maxHealth;
		
		int i = 0;
		int j = lvl;
		while(i < 3 && j >= 0)
		{
			if (stats.monsterDetails[speciesID].attackDetails[j] != 0)
			{
				attacks[i] = stats.monsterDetails[speciesID].attackDetails[j];
				i++;
			}
			j--;
		}
	}
	
	public void ReceiveAttack(int strength, int power, int element)
	{
		int damage = (int)(strength + power - toughness);
		
		if(element == resistance)
		{
			damage = damage / 2;
		}
		else if(element == weakness)
		{
			damage = damage * 2;
		}
		
		if(damage > 0)
		{
			currentHealth -= damage;
		}
	}
	
	public bool IsAlive()
	{
		if(currentHealth <= 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public void LevelUp()
	{
		lvl++;
		
		maxHealth = (int)(lvl * stats.monsterDetails[speciesID].healthMod);
		strength = (int)(lvl * stats.monsterDetails[speciesID].strengthMod);
		toughness = (int)(lvl * stats.monsterDetails[speciesID].toughMod);
		speed = (int)(lvl * stats.monsterDetails[speciesID].speedMod);
		resistance = (int)(lvl * stats.monsterDetails[speciesID].res);
		weakness = (int)(lvl * stats.monsterDetails[speciesID].weak);
		
		currentHealth = maxHealth;
	}
}

public class MonsterStats
{
	public MonsterData[] monsterDetails = new MonsterData[2];
	
	public MonsterStats()
	{
		//																						   		1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20
		monsterDetails[0] = new MonsterData("Penguisaurus", 4.4f, 1.0f, 1.2f, 1.0f, 2, 4, new int [20]	{6,	0,	0,	1,	0,	0,	3,	0,	0,	0,	0, 	0,	0,	0,	0,	0,	0,	0,	0,	0});
		monsterDetails[1] = new MonsterData("Tentacula", 4.2f, 1.2f, 1.0f, 1.2f, 1, 3, new int [20]		{1,	0,	0,	0,	5,	0,	0,	0,	0,	0,	0, 	0,	0,	0,	0,	0,	0,	0,	0,	0});
	}
}

public class MonsterData
{
	public string name;
	
	public float healthMod;
	public float strengthMod;
	public float toughMod;
	public float speedMod;
	
	public int res;
	public int weak;
	
	public int[] attackDetails = new int[20];
	
	public MonsterData(string n, float h, float st, float t, float sp, int r, int w, int[] atks)
	{
		name = n;
		healthMod = h;
		strengthMod = st;
		toughMod = t;
		speedMod = sp;
		res = r;
		weak = w;
		attackDetails = atks;
		
	}
}