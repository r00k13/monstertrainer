﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BattleManager : MonoBehaviour 
{
	public int battleState = 0;
	private string battleText;
	
	private Monster friendly;
	private Monster enemy;
	
	public Text infoText;
	public Text b1Text;
	public Text b2Text;
	public Text b3Text;
	public Text b4Text;
	public Text friendlyName;
	public Text friendlyHealth;
	public Text enemyName;
	public Text enemyHealth;
	
	public GameObject playerPos;
	public GameObject friendlyPos;
	public GameObject enemyPos;
	
	private GameObject enemyBattler;
	private GameObject friendlyBattler;
	
	public ParticleSystem part;
	
	public Material physMat;
	public Material flamMat;
	public Material aquaMat;
	public Material venoMat;
	public Material shadMat;
	public Material voltMat;
	
	public GameObject [] prefabs;
	public Attack[] attacks = new Attack[7];
	
	private MonsterManager mm;
	private GameManager gm;
	
	// Use this for initialization
	void Start () 
	{
		mm = gameObject.GetComponent<MonsterManager>();
		gm = gameObject.GetComponent<GameManager>();
		
		part.enableEmission = false;
		
		attacks[0] = new Attack(0, "Empty", 1, 0, 0, 0);             //creates the attacks that monsters will use
		attacks[1] = new Attack(1, "Breathe Fire", 0, 1, 6, 3);
		attacks[2] = new Attack(2, "Spark", 1, 5, 5, 2);
		attacks[3] = new Attack(3, "Scratch", 1 ,0, 2, 1);
		attacks[4] = new Attack(4, "Hydro Blast", 0 ,2, 6, 3);
		attacks[5] = new Attack(5, "Poison Strike", 1 ,3, 5, 2);
		attacks[6] = new Attack(6, "Dark Strike", 1, 4, 4, 2);
	}
	
	// Update is called once per frame
	void Update () 
	{	
		infoText.text = battleText;
		
		
		switch (battleState)
		{
		case 0:			//no battle
			break;
		case 1:			//initialise battle
			battleText = "A " + enemy.name + " attacked!";
			
			enemyBattler = Instantiate (prefabs[enemy.speciesID], enemyPos.transform.position, enemyPos.transform.rotation) as GameObject;	//instantiate the object
			friendlyBattler = Instantiate (prefabs[friendly.speciesID], friendlyPos.transform.position, friendlyPos.transform.rotation) as GameObject;
			
			battleState = 2;
			break;
		case 2: 		//root menu 
			battleText = "Choose an action";
			
			friendlyName.text = friendly.name + " Lvl: " + friendly.lvl;
			friendlyHealth.text = friendly.currentHealth + "/" + friendly.maxHealth;
			enemyName.text = enemy.name + " Lvl: " + enemy.lvl;
			enemyHealth.text = enemy.currentHealth + "/" + enemy.maxHealth;
			
			
			b1Text.text = "Fight";
			b2Text.text = "Switch";
			b3Text.text = "Capture";
			b4Text.text = "Flee";
			break;
		case 10:		//fight menu
			battleText = "Choose an attack";
			
			b1Text.text = attacks[friendly.attacks[0]].name;
			b2Text.text = attacks[friendly.attacks[1]].name;
			b3Text.text = attacks[friendly.attacks[2]].name;
			b4Text.text = "Cancel";
			break;
		case 11:		//friendly projectile attack     
			part.transform.Translate(Vector3.left * Time.deltaTime * 40);
			break;
		case 12:  		//enemy projectile attack
			part.transform.Translate(Vector3.right * Time.deltaTime * 40);
			break;
		case 13:		//friendly physical attack     
			friendlyBattler.transform.Translate(Vector3.forward * Time.deltaTime * 40);
			break;
		case 14:  		//enemy physical attack
			enemyBattler.transform.Translate(Vector3.forward * Time.deltaTime * 40);
			break;
		case 15:		//other attack     
			
			break;
		case 16:		//empty
			break;
		case 17:       //enemy defeated
			battleText = "The enemy was defeated";
			b1Text.text = "";
			b2Text.text = "";
			b3Text.text = "";
			b4Text.text = "Leave";
			Destroy(enemyBattler);
			break;
		case 18:       //one monster defeated
			battleText = "Your monster was defeated";
			b1Text.text = "";
			b2Text.text = "";
			b3Text.text = "";
			b4Text.text = "Flee";
			Destroy(friendlyBattler);
			break;
		case 19:       //all monsters defeated
			battleText = "All your monsters have been defeated";
			break;
		case 20:       //Switching
			//choose new monster
			break;
		case 21:                                                                                            //pause, then make the switch
			//summon new monster
			break;
		case 30:                                                                                           //capture attempt
			//attempt to capture
			break;
		case 31:                                                                                          //capture success
			//succesful capture
			break;
		case 32:                                                                                          //capture failure
			//failed capture
			break;
			
		}
	}
	
	public void StartBattle(int species)
	{
		friendly = mm.partyMonsters[0];
		enemy = new Monster(species, 5);
		battleState = 1;
	}
	
	public void EndBattle()
	{
		battleState = 0;
		Destroy(enemyBattler);
		Destroy (friendlyBattler);
		gm.EndBattle();
	}
	
	public void Button1()
	{
		if(battleState == 2)
		{
			battleState = 10;
		}
		else if(battleState == 10)
		{
			fight (attacks[friendly.attacks[0]]);
		}
	}
	
	public void Button2()
	{
		if(battleState == 2)
		{
			
		}
		else if(battleState == 10)
		{
			fight (attacks[friendly.attacks[1]]);
		}
	}
	
	public void Button3()
	{
		if(battleState == 2)
		{
			EndBattle ();
		}
		else if(battleState == 10)
		{
			fight (attacks[friendly.attacks[2]]);
		}
	}
	
	public void Button4()
	{
		if(battleState == 2 || battleState == 17 || battleState == 18)
		{
			EndBattle ();
		}
		else if(battleState == 10)
		{
			battleState = 2;
		}
	}
	
	void fight(Attack fAtk)
	{
		if(friendly.speed >= enemy.speed)
		{
			StartCoroutine(FriendlyAttack(fAtk, attacks[enemy.attacks[chooseEnemyAttack ()]]));
		}
		else
		{
			StartCoroutine(EnemyAttack(fAtk, attacks[enemy.attacks[chooseEnemyAttack ()]]));
		}
	}
	
	int chooseEnemyAttack()
	{
		int atk = 0;
		int index = 0;
		while(index == 0)
		{
			atk = Random.Range(0,3);
			if(enemy.attacks[atk] != 0)
			{	
				index = 1;
			}
		}
		return atk;
	}
	
	IEnumerator FriendlyAttack(Attack fAtk, Attack eAtk)
	{
		battleText = friendly.name + " used " + fAtk.name;
		if(fAtk.type == 0)
		{
			battleState = 11;
			LaunchProjectile(true, fAtk.element);
		}
		else if(fAtk.type == 1)
		{
			battleState = 13;
		}
		else
		{
			battleState = 15;
		}
		
		
		yield return new WaitForSeconds(1);
		
		friendlyBattler.transform.position = friendlyPos.transform.position;
		part.enableEmission = false;
		enemy.ReceiveAttack(friendly.strength, fAtk.power, fAtk.element);
		enemyHealth.text = enemy.currentHealth + "/" + enemy.maxHealth;
		
		if(enemy.IsAlive ())
		{
			if(friendly.speed >= enemy.speed)
			{
				StartCoroutine(EnemyAttack(fAtk, eAtk));
			}
			else
			{
				battleState = 2;
			}
		}
		else
		{
			battleState = 17;
		}
	}
	
	IEnumerator EnemyAttack(Attack fAtk, Attack eAtk)
	{
		battleText = enemy.name + " used " + eAtk.name;
		if(eAtk.type == 0)
		{
			battleState = 12;
			LaunchProjectile (false, eAtk.element);
		}
		else if(eAtk.type == 1)
		{
			battleState = 14;
		}
		else
		{
			battleState = 15;
		}
		
		
		yield return new WaitForSeconds(1);
		
		enemyBattler.transform.position = enemyPos.transform.position;
		part.enableEmission = false;
		friendly.ReceiveAttack(enemy.strength, eAtk.power, eAtk.element);
		friendlyHealth.text = friendly.currentHealth + "/" + friendly.maxHealth;
		
		if(friendly.IsAlive ())
			{
			if(friendly.speed < enemy.speed)
			{
				StartCoroutine(FriendlyAttack(fAtk, eAtk));
			}
			else
			{
				battleState = 2;
			}
		}
		else
		{
			if(mm.CheckSpares() > 0)
			{
				battleState = 18;
			}
			else
			{
				battleState = 19;
			}
		}
	}
	
	void LaunchProjectile(bool fromLeft, int element)
	{
		switch(element)
		{
		case 0:
			part.GetComponent<Renderer>().material = physMat;
			break;
		case 1:
			part.GetComponent<Renderer>().material = flamMat;
			break;
		case 2:
			part.GetComponent<Renderer>().material = aquaMat;
			break;
		case 3:
			part.GetComponent<Renderer>().material = venoMat;
			break;
		case 4:
			part.GetComponent<Renderer>().material = shadMat;
			break;
		case 5:
			part.GetComponent<Renderer>().material = voltMat;
			break;
		}
		
		part.enableEmission = true;
		
		if(fromLeft)
		{
			part.transform.position = friendlyPos.transform.position;
			
		}
		else
		{
			part.transform.position = enemyPos.transform.position;
		}
	}
}


public class Attack
{
	public int iD;
	public string name;
	public int type;					//0 = projectile, 1 = physical, 2 = other
	public int element;					//0 = physical, 1 = flame, 2 = aqua, 3 = venom, 4 = shadow, 5 = volt
	public int power;
	public int cost;
	
	public Attack(int id, string nam, int typ, int elem, int pow, int cos)         //constructor
	{
		iD = id;
		name = nam;
		type = typ;
		element = elem;
		power = pow;
		cost = cos;
	}
}