﻿using UnityEngine;
using System.Collections;

public class SpawnNPC : MonoBehaviour 
{
	public GameObject [] prefabs;
	
	public bool activated = false;
	
	public float spawnRate = 5.0f;
	private float nextSpawn = 0f;

	private GameManager gm;
	
	// Use this for initialization
	void Start () 
	{
		gm = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(activated && gm.gameState == 0)
		{
			if (Time.time > nextSpawn) 											//if current time is at least time a regen is due
			{
				nextSpawn = Time.time + spawnRate;								//set value of nextRegen to current time + time taken to regenerate
				if(gm.currentMonsters <= gm.maxMonsters)
				{
					createNPC (Random.Range(0,2), transform.position);
				}
			}
		}
		if(Input.GetKeyUp(KeyCode.X))
		{
			//createNPC (0, transform.position);
			activated = !activated;
			Debug.Log (activated);
		}
	}
	
	void createNPC(int type, Vector3 location)
	{
		GameObject obj = Instantiate (prefabs[type], location, prefabs[type].transform.rotation) as GameObject;	//instantiate the object
		obj.name = "Monster";									//give object a name
		
		gm.currentMonsters++;
	}
}
