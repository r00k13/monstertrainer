﻿using UnityEngine;
using System.Collections;

public class AnimateWater : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		//renderer.material.shader = Shader.Find("Transparent/Parallax Specular");
	}
	
	// Update is called once per frame
	void Update () 
	{
		float shininess = Mathf.PingPong(Time.time / 20, 0.03F);
		GetComponent<Renderer>().material.SetFloat("_Parallax", shininess);
		
		//Debug.Log(renderer.material.GetFloat("_Parallax"));
	}
}
