﻿using UnityEngine;
using System.Collections;

public class MonsterMove : MonoBehaviour 
{
	//private Vector3 startPoint;
	private Vector3 dir;
	public float speed = 10;
	private int dirAI = 0;
	public float distance;
	private bool chasing = false;
	public int aggroRange = 20;
	
	public int species;
	
	private CharacterController control;
	public GameObject charMesh;
	//public GameObject face;
	private PlayerMove playa;
	private GameManager gm;

	// Use this for initialization
	void Start () 
	{
		//startPoint = transform.position;
		control = gameObject.GetComponent<CharacterController>();
		playa = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlayerMove>();
		gm = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<GameManager>();
		//face.renderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(gm.gameState == 0)
		{
			Move();
		}
	}

	void OnTriggerEnter(Collider collision)
	{
		Vector3 direction = collision.gameObject.transform.position - transform.position;
		float angle = Vector3.Angle(direction, transform.forward);

		if(collision.gameObject.name == "Player" && angle < 60)
		{
			//Debug.Log ("Player at " + angle);
			chasing = true;
		}
	}
	
	void Move()
	{
		distance = Vector3.Distance (transform.position, playa.transform.position);
		
		if(distance < 8)
		{
			//Debug.Log ("Attack");
			if(charMesh != null)
			{
				Vector3 targetPostition = new Vector3(playa.transform.position.x, this.transform.position.y, playa.transform.position.z ) ;
				this.transform.LookAt( targetPostition ) ;
				charMesh.GetComponent<Animation>().CrossFade("Attack", 0.2F);
				Invoke ("StartBattle", 1);
			}
			else
			{
				Debug.Log ("Error: No Animation Found");
			}
		}
		else if (distance > aggroRange)
		{
			chasing = false;
		}
		else if (distance > 1000)
		{
			gm.currentMonsters--;
			Destroy(gameObject);
		}
		
		if(chasing)
		{
			Vector3 targetPostition = new Vector3(playa.transform.position.x, this.transform.position.y, playa.transform.position.z ) ;
			this.transform.LookAt( targetPostition ) ;
			//face.renderer.enabled = true;
		}
		else
		{
			//face.renderer.enabled = false;
			if((int)(Random.value * 20)  == 0)
			{
				dirAI = (int)(Random.value * 3);
			}
			
			if(dirAI == 0)
			{
				
			}
			else if(dirAI == 1)
			{
				transform.Rotate(0, 30f * Time.deltaTime, 0);
			}
			else
			{
				transform.Rotate(0, -30f * Time.deltaTime, 0);
			}
		}
		
		dir = transform.TransformDirection(Vector3.forward);
		control.SimpleMove(dir * speed);
		
		if(charMesh != null)
		{
			if((!charMesh.GetComponent<Animation>().isPlaying) && speed > 0)
			{
				charMesh.GetComponent<Animation>().Play ("Walk");
			}
		}
	}

	void StartBattle()
	{
		//Debug.Log ("Battle Start");
		gm.StartBattle(species, 0);
		gm.currentMonsters--;
		Destroy(gameObject);
	}
}
