﻿using UnityEngine;
using System.Collections;

public class AlphaFluctuate : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		float alpha = Mathf.PingPong(Time.time / 5, 0.3F);
		Color c = new Color(0,0,0,0);
		c = GetComponent<Renderer>().material.color;
		c.a = alpha;
		GetComponent<Renderer>().material.color = c;
	}
}
