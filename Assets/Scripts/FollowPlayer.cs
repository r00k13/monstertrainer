﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour 
{
	public int height;
	public int distance;
	
	public GameObject player;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 location = new Vector3(player.transform.position.x, player.transform.position.y + height, player.transform.position.z + distance);
		transform.position = location;
	}
}
